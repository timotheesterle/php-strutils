﻿<?php

class strUtils {
    private $_str;

    public function __construct($_str) {
        $this->_str = $_str;
    }

    public function bold() {
        return '<strong>'.$this->_str.'</strong>';
    }

    public function italic() {
        return '<em>'.$this->_str.'</em>';
    }

    public function underline() {
        return '<u>'.$this->_str.'</u>';
    }

    public function capitalize() {
        return strtoupper($this->_str);
    }

    public function uglify() {
        $this->_str = $this->bold();
        $this->_str = $this->italic();
        $this->_str = $this->underline();
        return $this->_str;
    }
}

$php = new strUtils('php is awesome<br>');
echo $php->bold();
echo $php->italic();
echo $php->underline();
echo $php->capitalize();
echo $php->uglify();

?>
